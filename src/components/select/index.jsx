import React from 'react';
import pt from 'prop-types';
import { Fragment, PureComponent } from 'react';
import uuid from 'uuid';
import cn from 'classnames';
import { Dropdown } from '../dropdown/';
import s from './styles.styl';
import { createOption, hasViewportBelow } from './utils';

const ELEMENT_COUNT = 6;
const ELEMENT_HEIGHT = 50;

export class Select extends PureComponent {
  static propTypes = {
    label: pt.node,
    value: pt.any
  };
  static defaultProps = {
    value: ''
  };
  static getDerivedStateFromProps({ label, mobile, options, value, onChange }) {
    return {
      showLabel: !!label,
      useNativeSelect: mobile,
      htmlOptions: mobile ? options.map(createOption) : null,
      dropdownContent: mobile ? null : options.map(x => <div>{x.label}</div>),
      selectedOption: value ? options.find(o => (o.value = value)) : {}
    };
  }
  state = {
    focused: false
  };
  uuid = uuid();
  id = `select-${this.uuid}`;
  labelId = `select-${this.uuid}-label`;
  wrapper = React.createRef();
  setFocused = () => this.setState({ focused: true });
  setUnfocused = () => this.setState({ focused: false });
  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.focused &&
      !prevState.focused &&
      !this.state.useNativeSelect
    ) {
      this.setState({
        dropDownOpen: hasViewportBelow(
          this.wrapper.current,
          ELEMENT_COUNT * ELEMENT_HEIGHT
        )
          ? 'bottom'
          : 'top'
      });
    } else if (this.state.dropDownOpen && !this.state.focused) {
      this.setState({
        dropDownOpen: false
      });
    }
  }
  render() {
    const {
      focused,
      dropDownOpen,
      showLabel,
      selectedOption,
      useNativeSelect
    } = this.state;
    const { onChange, label, value } = this.props;
    const labelMinimized = !!value || (focused && !useNativeSelect);
    const hasDropdown = focused && !useNativeSelect;
    const Wrapper = useNativeSelect ? Fragment : Dropdown;
    const wrapperProps = useNativeSelect
      ? null
      : { open: dropDownOpen, content: this.state.dropdownContent };
    return (
      <Wrapper {...wrapperProps}>
        <label
          className={cn({
            [s.wrapper]: true,
            [s.wrapper_focused]: focused,
            [s[`wrapper_flatCorners_${dropDownOpen}`]]: dropDownOpen
          })}
          htmlFor={this.id}
          ref={this.wrapper}
        >
          {useNativeSelect ? (
            <select
              aria-labelledby={this.labelId}
              className={s.select}
              id={this.id}
              value={selectedOption.value}
              onChange={onChange}
              onFocus={this.setFocused}
              onBlur={this.setUnfocused}
            >
              {this.state.htmlOptions}
            </select>
          ) : (
            <input
              aria-labelledby={this.labelId}
              className={s.input}
              id={this.id}
              defaultValue={selectedOption.label}
              onChange={onChange}
              onFocus={this.setFocused}
              onBlur={this.setUnfocused}
            />
          )}
          {!!(useNativeSelect && value.length) && (
            <span
              className={cn({
                [s.label]: true
              })}
            >
              <span
                className={cn({
                  [s.label__text]: true
                })}
              >
                {selectedOption.label}
              </span>
            </span>
          )}
          <span
            className={cn({
              [s.label]: true,
              [s.label_hidden]: !showLabel,
              [s.label_forInput]: !useNativeSelect,
              [s.label_minimized]: labelMinimized
            })}
            id={this.labelId}
          >
            <span className={s.label__text}>{label}</span>
            <span
              className={cn({
                [s.arrow]: true,
                [s.arrow_hidden]: labelMinimized
              })}
            />
          </span>
        </label>
      </Wrapper>
    );
  }
}
