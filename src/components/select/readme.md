Select example:

```js
<Select
  label="Выберите страну"
  options={[
    { value: 'US', label: 'США' },
    { value: 'UK', label: 'Великобритания' }
  ]}
/>
```

```js
<Select
  label="Выберите страну"
  options={[
    { value: 'US', label: 'США' },
    { value: 'UK', label: 'Великобритания' }
  ]}
  value="US"
  mobile
/>
```
