import React from 'react';

export function createOption({ value, label }) {
  return <option value={value}>{label}</option>;
}

export function hasViewportBelow(element, requiredHeight) {
  const box = element.getBoundingClientRect();
  return document.documentElement.clientHeight - box.bottom >= requiredHeight;
}
