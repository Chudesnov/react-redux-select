import React from 'react';
import pt from 'prop-types';
import { PureComponent } from 'react';
import cn from 'classnames';
import s from './styles.styl';

export class Dropdown extends PureComponent {
  static propTypes = {
    open: pt.bool,
    content: pt.node
  };
  renderDropdown() {
    return this.props.open ? (
      <div
        className={cn({
          [s.dropdown]: true,
          [s[`dropdown_${this.props.open}`]]: true
        })}
      >
        {this.props.content}
      </div>
    ) : null;
  }
  render() {
    return (
      <div className={s.wrapper}>
        {this.props.children}
        {this.renderDropdown()}
      </div>
    );
  }
}
