## Prerequisites

1.  Node 6 or higher
1.  A decent browser

## Installing dependencies

`npm install`

## Starting Styleguidist

`npm test`
